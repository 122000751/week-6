def recur_factorial(n, end):
    if n < 1 or end >= n:
       return 1
    else:
       return n*recur_factorial(n-1, end)

n, m, t = list(map(int, input().split()))
x = 0
for i in range(4, t):
    comb1 = recur_factorial(n, n - i) / recur_factorial(i, 1)
    comb2 = recur_factorial(m, m - (t - i)) / recur_factorial(t - i, 1)
    x += (int(comb1) * int(comb2))
print(x)